| Simulate the ALU control logic; test all the possible combinations
| Written by Pallav Gupta (pallav.gupta@villanova.edu)

vector alucontrol alucontrol[2] alucontrol[1] alucontrol[0]
vector aluop aluop[1] aluop[0]
vector funct funct[3] funct[2] funct[1] funct[0]

| test always ADD (funct are don't-care)
set aluop 00
set funct 0000
s 2
assert alucontrol 010
set aluop 01
s 2
assert alucontrol 110
set funct 0000
set aluop 10
s 2
assert alucontrol 010
set funct 0010
s 2
assert alucontrol 110
set funct 0100
s 2
assert alucontrol 000
set funct 0101
s 2
assert alucontrol 001
set funct 1010
s 2
assert alucontrol 111
| add the rest here
